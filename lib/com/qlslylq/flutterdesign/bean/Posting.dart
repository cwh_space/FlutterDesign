import 'package:flutter_design/com/qlslylq/flutterdesign/bean/AdmxObject.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/User.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/StringUtils.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/TextUtils.dart';

/*
 * 帖子<br/>
 */
class Posting extends AdmxObject {

  String title;

  String content;

  String tag;

  List<String> imgs;

  User user;

  Posting({this.tag, this.title, this.content});

  String getTitle() {
    if (TextUtils.isEmpty(title)) {
      return "这家伙很懒，没有加标题";
    }
    return title;
  }

  void setTitle(String title) {
    this.title = title;
  }

  String getContent() {
    return StringUtils.replaceEnter(content);
  }

  void setContent(String content) {
    this.content = content;
  }

  String getTag() {
    if (TextUtils.isEmpty(tag)) {
      return "懒签";
    }
    return tag;
  }

  void setTag(String tag) {
    this.tag = tag;
  }

  List<String> getImgs() {
    return imgs;
  }

  void setImgs(List<String> imgs) {
    this.imgs = imgs;
  }

  User getUser() {
    return user;
  }

  void setUser(User user) {
    this.user = user;
  }

  String getThumbContent() {
    String content = getContent();
    return content.length > 90 ? content.substring(0, 90) : content;
  }
}
