/*
 * Http键常量
 */
class HttpConstant {

  /*
   * 网络参数key
   */
  static final String API_METHOD = "method";
  static final String PARAM_TICK = "timestamp";
  static final String PARAM_NONCE = "nonce";
  static final String PARAM_PARTNER = "partner";
  static final String PARAM_SIGN = "signature";
  static final String PARAM_TOKEN = "session_token";
  static final String PARAM_USER_ID = "userid";
  static final String PARAM_APP_KEY = "appkey";
  static final String PARAM_PAGER_NO = "page";
  static final String PARAM_PAGER_SIZE = "rows";

  /*
   * 网络参数value
   */
  static String TOKEN = null;

}
