import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Parcel.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/widget/core/BaseListView.dart';

/*
 * 快递信息列表<br/>
 */
class ParcelListView extends BaseListView<Parcel> {

  const ParcelListView({Key key, @required list}) : super(key: key, list: list);

  State<StatefulWidget> createState() {
    return new ListViewState();
  }
}

class ListViewState extends BaseListViewState {

  ListTile createListTile(_item) {
    Parcel item = _item as Parcel;
    return new ListTile(
      title: new Text('${item.context}'),
    );
  }

}
